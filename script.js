import {beerCard, displayAllBeers, displayBeers} from "./modules/beer-cards.js";
import {setAbvFilter, compareToSearch, compareToAbv, compareToOrganic, filterUpdate, setCheckOrganicFilter, setSearchFilter} from "./modules/filters.js";

// VARIABLES + EVENT LISTENERS
var beers; // Ensemble des bières récupérées sur l'API
var mainList = []; // Liste de bières affichées / à afficher
var mainFilter = {
  search : "",
  maxAbv : 100,
  minAbv : 0
}; // stocke les paramètre de filtrage des bières

const mainContainer = document.getElementById('main-container'); // container où on affiche les bières

const searchbar = document.getElementById("searchbar"); // barre de recherche
searchbar.onkeyup = ()=>{setSearchFilter(mainFilter, searchbar, mainList, beers, mainContainer)};

const checkOrganic = document.getElementById('checkOrganic'); // checkbox  "is organic"
checkOrganic.onclick = ()=>{setCheckOrganicFilter(mainFilter, checkOrganic, mainList, beers, mainContainer)};

// Max et Min du slider abv
var maxAbvSlider = 0;
var minAbvSlider = 10;

function setAbvSlider(beers){
  for (let beer of beers) {
    if (parseFloat(beer.abv) > maxAbvSlider) {
      maxAbvSlider = parseFloat(beer.abv);
    };
    if (parseFloat(beer.abv) < minAbvSlider) {
      minAbvSlider = parseFloat(beer.abv);
    }
  };
}
// SLIDER DOUBLE ENTRÉE :
function createAbvSlider(){
  // https://www.cssscript.com/animated-customizable-range-slider-pure-javascript-rslider-js/
  new rSlider({
      target: '#abvslider',
      values : {min:minAbvSlider, max:maxAbvSlider},
      range: true,
      set : [minAbvSlider, maxAbvSlider],
      step: .5,
      scale : false,
      labels : false,
      onChange : (vals)=>{
              // vals est un string, donc on le coupe sur la virgule et on converti les valeurs obtenues en int
              let min = vals.split(',')[0];
              let max = vals.split(',')[1];
              setAbvFilter(mainFilter, max, min, mainList, beers, mainContainer);
        }
  });
};

// RÉCUPÉRER LES DONNÉES SUR LE ROUTER NODE.JS
const url = "http://127.0.0.1:3000/beers-all"; // URL du router

fetch(url)
  .then((response)=>{
    console.log("Request received");
    response.json().then((parsed)=>{
      console.log("parsed : ");
      console.log(parsed);
      beers = parsed; // stocker les bières dans beers
      //displayAllBeers(mainContainer, mainList, beers); // afficher toutes les bières
      setAbvSlider(beers); // fixer les extremums du slider en fonction des bières
      createAbvSlider();
      window.addEventListener("load",function(){
        for (let column of document.getElementsByClassName('flex-column')){
          console.log("ur in for");
          setTimeout(function(){
            column.classList.add('fadeInUp');
          }, 1000)
        };
      });
    })
  })
  .catch((error)=>{
    console.log("Erreur : "+error.message);
  })

// Ouverture du menu et barre de recherche

// Searchbar : .menu__searchbar, .menu__searchinput
// Menu : .menu, .menu__icon-search
var searchIsOpen = false;
const searchinput = document.querySelector('.menu__searchinput');
const searchBar = document.querySelector(".menu__searchbar");
const iconSearch = document.querySelector(".menu__icon-search");

function searchOpen(){
  searchinput.classList.add('menu__searchinput--open');
  searchBar.classList.add('menu__searchbar--open');
  iconSearch.classList.add('menu__icon-search--open');
  searchIsOpen = true;
};
function searchClose(){
  searchinput.classList.remove('menu__searchinput--open');
  searchBar.classList.remove('menu__searchbar--open');
  iconSearch.classList.remove('menu__icon-search--open');
  searchIsOpen = false;
}

document.querySelector('.menu__icon-search').onclick = ()=>{
  if (searchIsOpen) {
    searchClose();
  } else {
    searchOpen();
  }
};

var menuIsOpen = false;
const menuToggle = ()=>{
  let menu = document.querySelector(".menu");
  console.log("menuToggle");
  if (menuIsOpen) {
    menu.classList.remove('menu--animopen');
    menu.classList.remove('menu--animclose');
    document.querySelector('.menu__icon-search').classList.remove('menu__icon-search--open');
    for (let ele of document.querySelectorAll('.menu__filter-ele')) {
      ele.classList.add('menu__filter-ele-animclose');
    }
    setTimeout(()=>{
      menu.classList.remove('menu--open');
      menu.classList.add('menu--animclose');
      searchClose();
    }, 500);
    menuIsOpen = false;
  } else {
    for (let ele of document.querySelectorAll('.menu__filter-ele')) {
      ele.classList.remove('menu__filter-ele-animclose');
    }
    menu.classList.remove('menu--animopen');
    menu.classList.remove('menu--animclose');

    menu.classList.add('menu--animopen');
    searchOpen();
    setTimeout(()=>{
      menu.classList.add('menu--open');
      window.dispatchEvent(new Event('resize')); // Bug sur le slider : obligé de trigger un resize pour qu'il s'affiche correctement ...
    }, 500);
    menuIsOpen = true;
  };
};

document.querySelector(".menu__icon-filter").onclick = menuToggle;
