const axios = require('axios');
const http = require('http');
var beers;
var express = require('express');
var app = express();

// Récupérer les données de l'API avec axios :
function callAPI(pagenb){
  axios.get('https://sandbox-api.brewerydb.com/v2/beers?key=c134b747252ffb8354dfc0cd61ae5447', {
    params: {
      p: pagenb
    }})
  .then(response => {
    console.log("Request OK");
    console.log("PAGE "+response.data.currentPage);
    if (beers) {
      console.log("BEERS OK");
      for (beer of response.data.data) {
        beers.push(beer);
      }
    } else {
      beers = response.data.data;
    };
    if (pagenb < response.data.numberOfPages) {
      callAPI(pagenb+1);
    } else {
      console.log('DATA READY');
    }
  })
  .catch(error => {
    console.log("Erreur : "+error);
  });
};

// Routage
function startRoute(){
  app.get('/beers-all', function(req, res){
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Content-Type', "application/json");
      res.send(beers);
  });
  app.listen(3000);
}

callAPI(1);
startRoute()
