import {displayAllBeers, displayBeers} from "./beer-cards.js";

export function setAbvFilter(filter, maxVal, minVal, list, beers, container){
  filter.maxAbv = maxVal;
  filter.minAbv = minVal;
  filterUpdate(filter, list, beers, container);
};

export function compareToSearch(filter, beer){
  let typed = filter.search.toUpperCase();
  if (beer.name.toUpperCase().includes(typed)) {
    return true;
  } else if (filter.search == '') {
    return true;
  } else {
    return false;
  }
};

export function compareToAbv(filter, beer){
  let min = filter.minAbv;
  let max = filter.maxAbv;
  let abv = parseFloat(beer.abv);
  if (max >= abv && abv >= min ){
    return true;
  } else {return false;}
};

export function compareToOrganic(filter, beer){
  if (beer.isOrganic == "Y" && filter.isOrganic == true) {
    return true;
  } else if (beer.isOrganic == "N" && filter.isOrganic == false){
    return true;
  } else if (!filter.isOrganic){
    return true;
  } else {
    return false;
  }
}

// ***********************************************************************************
export function filterUpdate(filter, list, beers, container){
  list = [];
  // Comparaison avec toutes les bières
  for (let beer of beers) {
    if (compareToAbv(filter, beer) && compareToSearch(filter, beer) && compareToOrganic(filter, beer)) {
      list.push(beer);
    }
  }
  displayBeers(container, list);
}

export function setCheckOrganicFilter(filter, check, list, beers, container){
  filter.isOrganic = check.checked;
  filterUpdate(filter, list, beers, container);
};
export function setSearchFilter(filter, searchInput, list, beers, container){
  let timer;
  searchInput.addEventListener('keydown', ()=>{clearTimeout(timer)});
  searchInput.addEventListener('keyup', ()=>{timer = setTimeout(()=>{filter.search = searchbar.value; filterUpdate(filter, list, beers, container);}, 300)});
}
