// Component cartes de bières
// À REDEFINIR EN FONCION DU DESIGN

export function beerCard(beer){
  let label = "IMG/UnknownIMG.png";
  let description = "Too good to describe it !"
  if (beer.labels){
    label = beer.labels.medium;
  };
  if (beer.description){
    description = beer.description;
  };
  return `
  <div class="beer-card-container">
    <div class="beer-card">
      <div class="beer-card__icon">
        <div class="loader-container loader-container--visible">
          <div class="loader"></div>
        </div>
        <img class="beer-card__img" src="${label}" alt="${beer.name} logo">
      </div>
      <div class="beer-card__info-container">
        <div class="beer-card__info-texts-container">
          <h3 class="beer-card__name beer-card__name--small">${beer.nameDisplay}</h3>
          <p class="beer-card__info">Alcohol <span>${beer.abv}°</span></p>
          <p class="beer-card__info">Organic <span>${beer.isOrganic}</span></p>
          <div class="beer-card__description-container">
            <h4 class="beer-card__description-title">Description : </h3>
            <p class="beer-card__description-text">${description}</p>
          </div>
        </div>
      </div>
    </div>
  </div>
    `;
}

// Afficher toutes les bières
export function displayAllBeers(container, list, beers){
  console.log("DISPLAY ALL Beers");
  container.innerHTML = '';
  for (var i = 0; i < 50; i++) {
    list.push(beers[i]);
  };
  displayBeers(container, list);
}

// Insérer les bières dans le main-container
export function displayBeers(container, list){
  container.innerHTML = "";
  if (list.length > 0) {
      document.body.classList.remove('no-scroll');
    var reste = list.length%4;
    var divider = list.length - reste;
    var decalage = 0;
    console.log(reste + " " + divider);
    for(var i=0; i<=3; i++){
      let cardsColumn = "";
      // for (var j = 0; j < list.length/4; j++ ){
      //   if(j + i*list.length/4 <= list.length){
      //     let beer = list[j+i*(list.length/4];
      //     console.log(j+i*(list.length/4) + " " + list[j+i*(list.length/4)]);
      //     cardsColumn += beerCard(beer);
      //   }
      // }
      if (i==0){
        for(var j=0; j < divider/4; j++){
          let beer = list[j];
          //console.log(i+ " " + j + " " + list[j]);
          cardsColumn += beerCard(beer);
        }
        if(reste > 0){
          decalage ++;
          let beer = list[divider/4];
          cardsColumn += beerCard(beer);
          reste --;
        }
        container.innerHTML += '<div id="column-left" class="flex-column fadeInLeft-opening">'+cardsColumn+'</div>';
        setTimeout(function(){
          document.getElementById('column-left').style.opacity = '1';
          document.getElementById('column-left').classList.remove("fadeInLeft-opening");
        }, 2000)
      }
      else if (i==1){
        for(var j=divider/4 + decalage; j < 2*divider/4 + decalage; j++){
          let beer = list[j];
          cardsColumn += beerCard(beer);
          //console.log(i+ " " + j + " " + list[j]);
        }
        if(reste > 0){
          let beer = list[2*divider/4 + decalage];
          decalage ++;
          cardsColumn += beerCard(beer);
          reste --;
        }
        container.innerHTML += '<div id="column-down" class="flex-column fadeInDown-opening">'+cardsColumn+'</div>';
        setTimeout(function(){
          document.getElementById('column-down').style.opacity = '1';
          document.getElementById('column-down').classList.remove("fadeInDown-opening");
        }, 2000)
      }
      else if (i==2){
        for(var j=2*divider/4 + decalage; j < 3*divider/4 + decalage; j++){
          let beer = list[j];
          cardsColumn += beerCard(beer);
          //console.log(i+ " " + j + " " + list[j]);
        }
        if(reste > 0){
          let beer = list[3*divider/4 + decalage];
          decalage ++;
          cardsColumn += beerCard(beer);
          reste --;
        }
        container.innerHTML += '<div id="column-up" class="flex-column fadeInUp-opening">'+cardsColumn+'</div>';
        setTimeout(function(){
          document.getElementById('column-up').style.opacity = '1';
        document.getElementById('column-up').classList.remove("fadeInUp-opening");
      }, 2000)
      }
      else if (i==3){
        for(var j=3*divider/4+ decalage; j < divider+ decalage; j++){
          let beer = list[j];
          cardsColumn += beerCard(beer);
          //console.log(i+ " " + j + " " + list[j]);
        }
        container.innerHTML += '<div id="column-right" class="flex-column fadeInRight-opening">'+cardsColumn+'</div>';
        setTimeout(function(){
          document.getElementById('column-right').style.opacity = '1';
          document.getElementById('column-right').classList.remove("fadeInRight-opening");
        }, 2000);
      }
    }

    for (let card of document.getElementsByClassName('beer-card')) {
      card.parentNode.addEventListener('click',function(){
        if(card.classList.contains('beer-card__active')){
          document.body.classList.remove('no-scroll');
          card.parentNode.classList.add('fadeOut');
          card.parentNode.parentNode.style.marginTop = "";
          setTimeout(function(){
            card.parentNode.classList.remove('fadeOut');
            card.classList.remove('beer-card__active');
            card.parentNode.classList.remove('dark-background');
            card.childNodes[3].childNodes[1].childNodes[7].classList.remove("beer-card__description-container--active");
            card.classList.add('fadeInUp');

          }, 500);
          setTimeout(function(){
            card.classList.remove('fadeInUp');
          }, 1500);
        }
      });
      card.addEventListener('click',function(){
        if(!card.classList.contains('beer-card__active')){
            document.body.classList.add('no-scroll');
            card.parentNode.classList.add('fadeOut');
            card.parentNode.parentNode.style.marginTop = "100%";
          setTimeout(function(){
            card.childNodes[3].childNodes[1].childNodes[7].classList.add("beer-card__description-container--active");
            card.parentNode.classList.remove('fadeOut');
            card.parentNode.classList.add('fadeIn');
            card.classList.add('beer-card__active');
            card.parentNode.classList.add('dark-background');
            card.classList.add('fadeInUp');
          }, 1000);
          setTimeout(function(){
            card.classList.remove('fadeInUp');
            card.parentNode.classList.remove('fadeIn');
          }, 1500);
        }
      });
    }
    // Cacher le loader quand l'image est chargée
    for (let ele of document.getElementsByClassName("beer-card__img")) {
      ele.addEventListener("load", ()=>{ele.previousSibling.previousSibling.classList.remove("loader-container--visible")});
    }
  } else {
    container.innerHTML = `<h2 class="noresult">No result</h2>`;
  }
}