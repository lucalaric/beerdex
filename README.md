# Beerdex, le premier pokédex des bières

Projet développé par Luc Pinguet et Alaric Rougnon-Glasson dans le cadre du cours de web de première année d'IMAC, les ingénieurs créatifs.

## Get started

Start the nodejs script by using `node ./node-request.js` and wait for it to fully load the data (`DATA READY` written in the console).
You need to run the project from an apache server, xampp or easyphp for example.
